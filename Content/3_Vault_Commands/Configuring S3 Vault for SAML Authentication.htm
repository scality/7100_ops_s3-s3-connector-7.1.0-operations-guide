﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="5" MadCap:lastHeight="868" MadCap:lastWidth="648">
    <head>
        <link href="../Resources/TableStyles/SimpleWithPadding.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <h1>Configuring S3 Vault for SAML Authentication</h1>
        <p style="text-align: left;">To use the SAML&#160;Authentication feature, there must be a "saml" section in the S3 Vault <cite>config.json</cite> file, and it must include the Identity Provider's URL and the S3 Vault endpoint for processing authentication requests. </p>
        <p style="text-align: left;">This "saml" section may optionally include secret key and session expiration times.</p>
        <p class="codeparatext">"saml": {</p>
        <p class="codeparatext_indent">"entryPoint": "https://&lt;identity-provider-url&gt;",</p>
        <p class="codeparatext_indent">"issuerHost": "https://&lt;vault-host-url&gt;",</p>
        <p class="codeparatext_indent">"issuerPort": &lt;vault-host-port&gt;,</p>
        <p class="codeparatext_indent">"secretKeyExpirationHours": &lt;integer, optional, default=6&gt;,</p>
        <p class="codeparatext_indent">"sessionExpirationHours": &lt;integer, optional, default=6&gt;</p>
        <p class="codeparatext_lastline">},</p>
        <p class="Note" style="text-align: left;">The <cite>config.json</cite> file is configured automatically if the SAML authentication information is set in the "saml" section of the <cite>env/{{my_setup_env}}/group_vars/all</cite> template, and any comment tags are removed from the section before installing the <MadCap:variable name="S3 Connector Variables.ComponentName" />.</p>
        <p style="page-break-after: avoid;text-align: left;">Configurable parameters for the S3 Vault <cite>config.json</cite> file "saml" include:</p>
        <table style="mc-table-style: url('../Resources/TableStyles/SimpleWithPadding.css');width: 100%;" class="TableStyle-SimpleWithPadding" cellspacing="0">
            <col class="TableStyle-SimpleWithPadding-Column-Column1" style="width: 100px;" />
            <col class="TableStyle-SimpleWithPadding-Column-Column2" style="width: 542px;" />
            <thead>
                <tr class="TableStyle-SimpleWithPadding-Head-Header1">
                    <th class="TableStyle-SimpleWithPadding-HeadE-Column1-Header1">"saml" Parameter</th>
                    <th class="TableStyle-SimpleWithPadding-HeadD-Column2-Header1">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr class="TableStyle-SimpleWithPadding-Body-Body1">
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column1-Body1">entryPoint</td>
                    <td class="TableStyle-SimpleWithPadding-BodyD-Column2-Body1">URL&#160;of the SSO Identity Provider</td>
                </tr>
                <tr class="TableStyle-SimpleWithPadding-Body-Body1">
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column1-Body1">issuerHost</td>
                    <td class="TableStyle-SimpleWithPadding-BodyD-Column2-Body1">Host exposed by S3 Vault to the user for processing authentication requests.</td>
                </tr>
                <tr class="TableStyle-SimpleWithPadding-Body-Body1">
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column1-Body1">issuerPort</td>
                    <td class="TableStyle-SimpleWithPadding-BodyD-Column2-Body1">Port exposed by S3 Vault to the user for processing authentication requests.</td>
                </tr>
                <tr class="TableStyle-SimpleWithPadding-Body-Body1">
                    <td class="TableStyle-SimpleWithPadding-BodyE-Column1-Body1">secretKeyExpirationHours</td>
                    <td class="TableStyle-SimpleWithPadding-BodyD-Column2-Body1">(Optional) Period of time, in hours, during which the access key received will be valid for authentication.&#160;Default: <span class="ProperNameBold">6</span>.</td>
                </tr>
                <tr class="TableStyle-SimpleWithPadding-Body-Body1">
                    <td class="TableStyle-SimpleWithPadding-BodyB-Column1-Body1">sessionExpirationHours</td>
                    <td class="TableStyle-SimpleWithPadding-BodyA-Column2-Body1">(Optional) Period of time, in hours, during which a cookie returned after authentication by S3 Vault remains valid. Default: <span class="ProperNameBold">6</span>.</td>
                </tr>
            </tbody>
        </table>
        <p style="text-align: left;">It is necessary to whitelist <code>&lt;issuerHost&gt;:&lt;issuerPort&gt;</code> in the Identity Provider. S3 Vault will not provide SAML-based authentication if a correct configuration is not found in the <cite>config.json</cite> file.</p>
        <p style="text-align: left;">If using a secret key, once the secret key has expired for a particular user, that user will need to send a new request to S3 Vault and authenticate user privileges through the Identity Provider to obtain a different access key.</p>
        <h2>Example S3 Vault Configuration File</h2>
        <p class="codeparatext">{</p>
        <p class="codeparatext_indent">"address": "127.0.0.1",</p>
        <p class="codeparatext_indent">"dbPath": "databases/",</p>
        <p class="codeparatext_indent">"port": 8500,</p>
        <p class="codeparatext_indent">"map" : [</p>
        <p class="codeparatext_indent_indent">{ "ip": "127.0.0.1", "port": 4242 }</p>
        <p class="codeparatext_indent">],</p>
        <p class="codeparatext_indent">"keyFilePath": "/etc/scality/vault/keyfile",</p>
        <p class="codeparatext_indent">"identityServerPort": 3030,</p>
        <p class="codeparatext_indent">"saml": {</p>
        <p class="codeparatext_indent_indent">"entryPoint": "https://testAdfsServer/adfs/ls",</p>
        <p class="codeparatext_indent_indent">"issuerHost": "https://127.0.0.1",</p>
        <p class="codeparatext_indent_indent">"issuerPort": 3030</p>
        <p class="codeparatext_indent">},</p>
        <p class="codeparatext_indent">"log": {</p>
        <p class="codeparatext_indent_indent">"host": "127.0.0.1",</p>
        <p class="codeparatext_indent_indent">"port": 5505,</p>
        <p class="codeparatext_indent_indent">"level": "info",</p>
        <p class="codeparatext_indent_indent">"dump": "error"</p>
        <p class="codeparatext_indent">}</p>
        <p class="codeparatext_lastline">}</p>
        <h2>Creating a Domain Account</h2>
        <p style="text-align: left;">Before authenticating against an ADFS server or SSO&#160;application, users must create an account in S3 Vault with the same name as the ADFS/SSO domain name. Newly created S3 Vault users are assigned to this account, just as ADFS/SSO users belong to their respective domains. User authentication attempts belonging to domains with no equivalent account in S3 will result in an error.</p>
        <p style="text-align: left;">The domain account can be created in S3 Vault via the vaultclient command.</p>
        <p class="codeparatext_lastline" style="text-align: left;">$ bin/vaultclient create-account --name &lt;ADFS-domain-name&gt; --email &lt;email@example.com&gt; --host &lt;address:port&gt;</p>
        <p class="Note" style="text-align: left;">&lt;ADFS-domain-name&gt; can include spaces through the use of double quotation marks (e.g., "Account 1").</p>
        <p style="text-align: left;">Note that the <code>--host </code>parameter points to S3 Vault's traditional endpoint given by <code>address:port</code> rather than the SAML authentication endpoint given by <code>issuerHost:issuerPort.</code></p>
    </body>
</html>